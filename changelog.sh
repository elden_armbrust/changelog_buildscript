#!/usr/bin/env bash
APPLICATION_TITLE="IDS Serverless Property Service"
REPOSITORY_URL=https://bitbucket.org/investability/cardservice
LATEST_TAG="0.0.4"
PREVIOUS_TAG="0.0.1"
# DO NOT EDIT BELOW THIS LINE. DEAD MEN TELL NO TALES.
COMMITS=$(git log $PREVIOUS_TAG..$LATEST_TAG --pretty=format:"%H")
MARKDOWN="# "
MARKDOWN+=$APPLICATION_TITLE
MARKDOWN+='\n'
CURRENT_TAG="0.0.0"
for COMMIT in $COMMITS; do
    TAG=$(git describe ${COMMIT} --abbrev=0)
    if [ "$CURRENT_TAG" != "$TAG" ]; then
        MARKDOWN+="## "
        MARKDOWN+=$TAG
        MARKDOWN+='\n'
        CURRENT_TAG=$TAG
    fi
    SUBJECT=$(git log -1 ${COMMIT} --pretty=format:"%s")
    MARKDOWN+='- '
    MARKDOWN+=$SUBJECT
    MARKDOWN+='\n'
done
echo -e $MARKDOWN > changelog.md